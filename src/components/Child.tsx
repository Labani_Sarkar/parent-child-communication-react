import React from 'react'
const { forwardRef, useState, useImperativeHandle } = React;
const Child =forwardRef((props, ref) => {
    const [state, setState] = useState(0);

    const getAlert = () => {
      alert("getting alert from child component");
      setState(state => state + 1)
    };
    useImperativeHandle(ref, () => ({
      onParentClick(){
        getAlert()
      }
      }));
    return (
        <div><h1>Child component</h1>
        <h1>Count {state}</h1>
        <br />
      </div>
    )
});

export default Child
