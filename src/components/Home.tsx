import React from 'react'
import Parent from './Parent'

const Home = () => {
    return (
        <div className="div-content">
        <h1>Home</h1>
        <p>Rendering Parent component inside Home </p>
        <Parent title={"Description from Home component!"} />
        </div>
    )
}

export default Home
