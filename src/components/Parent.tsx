import React from 'react'
import Child from './Child';
const {  useRef } = React;
const Parent = (props:any) => {
    const childRef:any = useRef();
    const onClick =()=>{
        if (childRef.current) {
            childRef?.current?.onParentClick();
          }
    }
    return (
        <div className="div-content">
        <h1>Parent component</h1>
        <p>Hello World</p>
        <b>{props.title}</b><br />
       <div>Parent component! click to get alert from Child Component <button onClick={onClick} >Click Child</button></div> 
        <Child ref={childRef} />
        </div>
    )
}

export default Parent
