import './App.css';
import { BrowserRouter as Router, Route, Routes , Navigate } from "react-router-dom";
import Home from './components/Home';

function App() {
  return (
    <Router>
    <Routes>
       <Route path="/" element={<Home />} />
       </Routes>
       
         {/* <Route path="/news">
           <NewsFeed />
         </Route> */}
     </Router>
  );
}

export default App;
